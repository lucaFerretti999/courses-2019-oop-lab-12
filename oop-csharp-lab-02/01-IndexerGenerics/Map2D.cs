﻿using System;
using System.Collections.Generic;

namespace IndexerGenerics
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        /*
         * Suggerimento: come struttura dati interna per la memorizzazione dei valori della mappa
         * si suggerisce di utilizzare un Dictionary generico contenente come chiave una tupla di due
         * valori (le chiavi della mappa) e come valore il valore della mappa.
         * 
         * Esempio:
         * private IDictionary<Tuple<TKey1,TKey2>,TValue> values;
         */

        private IDictionary<Tuple<TKey1, TKey2>, TValue> values;
        private int count = 0;
        public Map2D()
        {
            values = new Dictionary<Tuple<TKey1, TKey2>, TValue>();
    }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            /*
             * Nota: si verifichi il funzionamento del metodo Invoke() su oggetti di classe Func
             */
            foreach (TKey1 key1 in keys1)
            {
                foreach (TKey2 key2 in keys2)
                {
                    Tuple<TKey1, TKey2> tupla = Tuple.Create(key1, key2);
                    TValue value = generator.Invoke(key1, key2);
                    values.Add(tupla, value);
                    count++;
                }
            }
        }

        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            if (other.ToString() == this.ToString())
                return true;
            return false;
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            /*
             * Suggerimento: si noti che sulla classe Dictionary è definito un indicizzatore per i valori di chiave
             */

            get
            {
                var key = Tuple.Create(key1, key2);
                return values[key];
            }

            set
            {
                var key = Tuple.Create(key1, key2);
                values[key] =  value;
            }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            var row = new List<Tuple<TKey2, TValue>>();
            foreach(Tuple<TKey1,TKey2> tupla in values.Keys)
            {
                if(tupla.Item1.Equals(key1))
                {
                    var couple =Tuple.Create(tupla.Item2, values[tupla]);
                    row.Add(couple);
                }
            }

            return row;
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            var column = new List<Tuple<TKey1, TValue>>();
            foreach (Tuple<TKey1, TKey2> tupla in values.Keys)
            {
                if (tupla.Item1.Equals(key2))
                {
                    var couple = Tuple.Create(tupla.Item1, values[tupla]);
                    column.Add(couple);
                }
            }

            return column;
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            IList<Tuple<TKey1, TKey2, TValue>> elems = new List<Tuple<TKey1, TKey2, TValue>>();
            foreach (Tuple<TKey1,TKey2> key in values.Keys)
            {
                var tris = Tuple.Create(key.Item1, key.Item2, values[key]);
                elems.Add(tris);                
            }   
            return elems;
        }

        public int NumberOfElements
        {
            get
            {
                return count;
            }
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
